import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

import Header from '../components/Header.js'
import HomePage from '../components/HomePage'
import BlogListPage from '../components/BlogListPage.js'
import BlogDetailsPage from '../components/BlogDetailsPage.js'
import ContactPage from '../components/ContactPage.js'
import NotFoundPage from '../components/NotFoundPage.js'

const AppRouter = () => {
    return (
        <BrowserRouter>
            <div>
                <Header></Header>
                <Routes>
                    <Route path='/' element={<HomePage />} exact />
                    <Route path='/blogs' element={<BlogListPage />} />
                    <Route path='/blogs/:id' element={<BlogDetailsPage />} />
                    <Route path='/contact' element={<ContactPage />} />
                    <Route element={<NotFoundPage />} />
                </Routes>
            </div>
        </BrowserRouter>
    )
}

export default AppRouter